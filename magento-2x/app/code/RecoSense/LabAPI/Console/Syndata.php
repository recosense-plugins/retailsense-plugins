<?php
namespace RecoSense\LabAPI\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use RecoSense\LabAPI\Model\RsenceproductFactory;

class Syndata extends Command
{
	/**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
	public function __construct(
        //Context $context,
        RsenceproductFactory $modelRsenceproductFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \RecoSense\LabAPI\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct();
        $this->_modelRsenceproductFactory = $modelRsenceproductFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->_messageManager = $messageManager;
        $this->date = $date;
        $this->helper = $helper;
    }	
   protected function configure()
   {
       $this->setName('SyncData:RecoSense');
       $this->setDescription('Partially sync items to RecoSense');
       parent::configure();
   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('labapi_rsenceproduct'); //gives table name with prefix
		 
		$sql = 'INSERT INTO ' . $tableName .' (product_id,action,status,created_at) SELECT entity_id as product_id, "insert" as action,0 as status,now() as created_at FROM catalog_product_entity WHERE NOT EXISTS (SELECT * FROM labapi_rsenceproduct WHERE labapi_rsenceproduct.product_id = catalog_product_entity.entity_id)';
		$connection->query($sql);
	}
}
