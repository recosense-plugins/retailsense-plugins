<?php
namespace RecoSense\LabAPI\Console;



//use Magento\Backend\App\Action;
//use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use RecoSense\LabAPI\Model\RsenceproductFactory;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncdatatoRecoSense extends Command
{
	/**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;
	private $state;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
	public function __construct(
        //Context $context,
        \Magento\Framework\App\State $state,
        RsenceproductFactory $modelRsenceproductFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \RecoSense\LabAPI\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct();
        $this->state = $state;
        $this->_modelRsenceproductFactory = $modelRsenceproductFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->_messageManager = $messageManager;
        $this->date = $date;
        $this->helper = $helper;
    }	
   protected function configure()
   {
       $this->setName('PushItem:RecoSense');
       $this->setDescription('Push all items to RecoSense');
       
       parent::configure();
   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {
		$this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
		$RsenceproductModel = $this->_modelRsenceproductFactory->create();
		$collections = $RsenceproductModel->getCollection()->addFieldToFilter('status',array('eq' =>0));
		if(count($collections)>0){
			if(count($collections)>0){
				$records = count($collections);
				$loop = ceil($records/100);
				for($i=1;$i<=$loop;$i++){
					echo $i;
					$successMessage = $this->helper->pushItem();
				}
			}else{
				$successMessage = $this->helper->pushItem();
			}
		}
		

   }
}
