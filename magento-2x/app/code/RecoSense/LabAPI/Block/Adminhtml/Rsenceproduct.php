<?php
namespace RecoSense\LabAPI\Block\Adminhtml;
class Rsenceproduct extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_rsenceproduct';/*block grid.php directory*/
        $this->_blockGroup = 'RecoSense_LabAPI';
        $this->_headerText = __('Rsenceproduct');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();
        
        $this->buttonList->remove('add');
    }
}
