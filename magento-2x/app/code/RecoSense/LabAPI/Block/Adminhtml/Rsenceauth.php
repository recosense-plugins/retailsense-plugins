<?php
namespace RecoSense\LabAPI\Block\Adminhtml;
class Rsenceauth extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_rsenceauth';/*block grid.php directory*/
        $this->_blockGroup = 'RecoSense_LabAPI';
        $this->_headerText = __('Rsenceauth');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();
		$this->buttonList->remove('add');
    }
}
