<?php
namespace RecoSense\LabAPI\Block\Adminhtml\Rsenceauth\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_rsenceauth_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Rsenceauth Information'));
    }
}