<?php
namespace RecoSense\LabAPI\Controller\Adminhtml\Syncdata;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use RecoSense\LabAPI\Model\RsenceproductFactory;

class Index extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        RsenceproductFactory $modelRsenceproductFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        //\Magento\Framework\Message\ManagerInterface $messageManager,
        \RecoSense\LabAPI\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->_modelRsenceproductFactory = $modelRsenceproductFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->resultPageFactory = $resultPageFactory;
        //$this->_messageManager = $messageManager;
        $this->date = $date;
        $this->helper = $helper;
    }

    public function execute()
    {
		shell_exec('php bin/magento SyncData:RecoSense > /dev/null 2>/dev/null &');
		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('adminhtml/system_config/edit/section/recosensesection');
        return $resultRedirect;
		
    }
}
