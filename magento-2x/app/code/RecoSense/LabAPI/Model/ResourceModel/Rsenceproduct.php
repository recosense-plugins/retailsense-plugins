<?php
/**
 * Copyright © 2015 RecoSense. All rights reserved.
 */
namespace RecoSense\LabAPI\Model\ResourceModel;

/**
 * Rsenceauth resource
 */
class Rsenceproduct extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('labapi_rsenceproduct', 'entity_id');
    }

  
}
