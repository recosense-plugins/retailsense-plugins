<?php
/**
 * Copyright © 2015 RecoSense. All rights reserved.
 */
namespace RecoSense\LabAPI\Model\ResourceModel;

/**
 * Rsenceauth resource
 */
class Rsenceauth extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('labapi_rsenceauth', 'entity_id');
    }

  
}
