<?php
/**
 * Copyright © 2015 RecoSense. All rights reserved.
 */

namespace RecoSense\LabAPI\Model;

use Magento\Framework\Exception\RsenceauthException;

/**
 * Rsenceauthtab rsenceauth model
 */
class Rsenceauth extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init('RecoSense\LabAPI\Model\ResourceModel\Rsenceauth');
    }

   
}
