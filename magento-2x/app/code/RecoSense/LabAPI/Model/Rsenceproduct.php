<?php
/**
 * Copyright © 2015 RecoSense. All rights reserved.
 */

namespace RecoSense\LabAPI\Model;

use Magento\Framework\Exception\RsenceproductException;
/**
 * Rsenceproducttab rsenceproduct model
 */
class Rsenceproduct extends \Magento\Framework\Model\AbstractModel
{

    public function _construct()
    {
        $this->_init('RecoSense\LabAPI\Model\ResourceModel\Rsenceproduct');
    }

   
}
