<?php
/**
 * Copyright © 2015 RecoSense . All rights reserved.
 */
namespace RecoSense\LabAPI\Helper;
use RecoSense\LabAPI\Model\RsenceproductFactory;
use RecoSense\LabAPI\Model\RsenceauthFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_productCollectionFactory;
	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	public function __construct(
		//\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		RsenceproductFactory $modelRsenceproductFactory,
		RsenceauthFactory $modelRsenceauthFactory,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Catalog\Model\ProductFactory $_productloader,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\CatalogInventory\Api\StockStateInterface $stockItem,
		\Magento\Review\Model\ResourceModel\Review\CollectionFactory $ratingCollection,    	
    	\Magento\Store\Model\StoreManagerInterface $storeManager,
    	\Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory $groupCollectionFactory,
		\Magento\Framework\App\Helper\Context $context
	) {
		$this->scopeConfig = $context;
		$this->_modelRsenceproductFactory = $modelRsenceproductFactory;
		$this->_modelRsenceauthFactory = $modelRsenceauthFactory;
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->_productloader = $_productloader;
		$this->_resourceConnection = $resourceConnection;
		$this->stockItem = $stockItem;
		$this->ratingCollection=$ratingCollection;
		$this->_storeManager = $storeManager;
		$this->_ratingFactory = $ratingFactory;
        $this->categoryRepository = $categoryRepository;
        $this->date = $date;
        $this->groupCollectionFactory = $groupCollectionFactory;
		parent::__construct($context);
	}
	
	public function authentication(){
				
				$curl = curl_init();
				$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
				$enable_module = $this->scopeConfig->getValue('recosensesection/recosensegroup/active', $storeScope);
				$client_id = $this->scopeConfig->getValue('recosensesection/recosensegroup/clientid', $storeScope);
				$client_secret = $this->scopeConfig->getValue('recosensesection/recosensegroup/clientsecret', $storeScope);
				
				$grant_type = 'auth_token';
				
				if( $enable_module == 0 || $client_id == '' || $client_secret == '' || $grant_type == '' ){
					return NULL;
				}

				$post_data = array(
						'client_id' => $client_id,
						'client_secret' => $client_secret,
						'grant_type' => $grant_type
					);	
				 
				 $headers = array(
					"content-type: application/json"
				  );


				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://retail-api.recosenselabs.com/v1.1/oauth2",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => $post_data
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				$rspAry = array();

				if ($err) {
				  $rspAry = "cURL Error #:" . $err;
				} else {
				  $rspAry = json_decode($response, true);
				}

				return $rspAry;
	}
	public function getProductIds(){
		
		$RsenceproductModel = $this->_modelRsenceproductFactory->create();
		$collections = $RsenceproductModel->getCollection()
											->addFieldToFilter('status',array('eq' =>0))
											->addFieldToFilter('action',array('neq' =>'delete')); //Get Collection of module data
		$collections->setPageSize(100)->setCurPage(1);
		if(count($collections)>0){
			foreach($collections as $collection){
				$products[] = $collection->getProductId();
			}
			return $products;
		}else{
			return NULL;
		}
	}
	public function productjson(){
				
				$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
				$productIds = $this->getProductIds();
				if(empty($productIds)){
					return NULL;
				}

				$_productCollection = $this->_productCollectionFactory->create();
				$_productCollection->addAttributeToSelect('*');
				$_productCollection->addAttributeToFilter('entity_id', array('in'=> $productIds));
				
				//echo '<pre>'; print_r($productIds); exit;

										
				$productdata= array();
				
				$client_id = $this->scopeConfig->getValue('recosensesection/recosensegroup/clientid', $storeScope);
				
				$productdata['client_id'] = $client_id;
				
				foreach ($_productCollection as $_product){
					//echo $_product->getId(); exit;
					$product =  $this->_productloader->create()->load($_product->getId());
					
					$productType = $product->getTypeId();
					$categoryIds = $product->getCategoryIds();
					
					$categories = $this->getcategory($product);
					
					$parentIdArray="";					
					$childProducts =array();					
					$attributecollection = array();
					
					if($productType == 'configurable'){  
						
						$childs = $product->getTypeInstance()->getUsedProducts($product);
						
							foreach($childs as $value){
								$childid = (int)$value['entity_id'];
								$childProducts[]= (int)$value['entity_id'];
								$parentid = $product->getId();
							}
						
						$productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
						
						foreach($productAttributeOptions as $options)
						{
							
  							$attributeid = $options['attribute_id'];
 							$attributelabel = $options['frontend_label'];
 							$attoptios = $options['values'];
							foreach($attoptios as $option)
							{
								$attributeoption = $option['label'];
								$attributecollection[] = array(
													"label"=> $attributelabel,
													"value"=>$attributeoption,
													"id" =>$attributeid
												);	
							}
 						}
						//echo '<pre>';print_r($attributecollection); exit;
					}
					
					$attributeSetId = $product->getAttributeSetId();
					$groupCollection = $this->groupCollectionFactory->create()
						->setAttributeSetFilter($attributeSetId)
						->setSortOrder()
						->load();
					$productattributes = array();
					foreach ($groupCollection as $group) {						
						$attributes = $product->getAttributes($group->getId(), true);						
						foreach ($attributes as $key => $attribute) {
							if($attribute->getIsVisibleOnFront() && $attribute->getFrontend()->getValue($product) !="" && $attribute->getFrontend()->getValue($product) !="Non" && $attribute->getFrontend()->getValue($product) !="No"){
								$productattributes[] = array(
													"label"=> $attribute->getFrontend()->getLabel(),
													"value"=>$attribute->getFrontend()->getValue($product),
													"id" =>$attribute->getId()
												);
							}
						}
					}
					if (empty($attributecollection)) {
						$productattributes = $productattributes;
					}else{
						$productattributes = array_merge($productattributes,$attributecollection);
					}
					
					$this->_connection = $this->_resourceConnection->getConnection();		
					
					$query = "SELECT * FROM `catalog_product_super_link` where product_id = ".$product->getId()." Limit 0,1"; 
					$products = $this->_connection->fetchAll($query);
					
					$parent_id = 0;
					if(count($products))
					{
						$parent_id = $products[0]['parent_id'];
					}
					$tagcollect = array();
					$qty = (int) $this->stockItem->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
 					$product_links = array($product -> getProductUrl());
					
					$ratingCollection = $this->ratingCollection->create()
											->addStoreFilter($this->_storeManager->getStore()->getId())
											->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
											->addEntityFilter('product',$product->getId())
											->setDateOrder()
											->addRateVotes();
					$avg = 0;
					$ratings = array();
					$ratingPercent = 0;
					
					if (count($ratingCollection) > 0){						
						foreach ($ratingCollection->getItems() as $_review):
							foreach( $_review->getRatingVotes() as $_vote ):
								$ratings[] = $_vote->getPercent();
							endforeach;
						endforeach;
						 $avg = array_sum($ratings)/count($ratings);
						$ratingPercent = (float)number_format($avg / 20,1);
					 }
					 
					$visibility = $product->getVisibility();
                    if($visibility == 4){
                         $visibility = array('visible_in_catalog'=>true,'visible_in_search'=>true);
                    }else if($visibility == 3){
                         $visibility = array('visible_in_catalog'=>false,'visible_in_search'=>false);    
                    }else if($visibility == 2){
                         $visibility = array('visible_in_catalog'=>true,'visible_in_search'=>false);    
                    }else if($visibility == 1){
                         $visibility = array('visible_in_catalog'=>false,'visible_in_search'=>false);    
                    }
					$currentStore = $this->_storeManager->getStore();
					$mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'catalog/product';
					
					$productdata['items'][]= 
					array(
						"item_id"=>$_product->getId(),
						"title"=>$_product->getName(),
						"long_description"=>$_product->getDescription(),
						"short_description"=>$_product->getShortDescription(),
						"parent_id"=> (int)$parent_id,
						"associated_item_ids"=> $childProducts,
						"sku"=> $_product->getSku(),
						"price"=> (float)$_product->getFinalPrice(),
						"category_trees"=>$categories,	
						"visibility"=>$visibility,
						"product_links" =>$product_links,
						"product_attributes"=> $productattributes,
						"rating"=> $ratingPercent,
						"tags"=>$tagcollect,
						"in_stock"=> $qty,
						"images"=> array(
							"base_image"=> $mediaUrl.$product->getData('image'),
							"small_image"=> $mediaUrl.$product->getData('small_image'),
							"thumbnail"=>  $mediaUrl.$product->getData('thumbnail'),
							"featured_image"=> $this->getFeaturedImage($product)
						),
					);
					$productdata['integration']= "m2x";
					$productdata['action']= "update";
				   
				}
				//echo json_encode($productdata); exit;
				return json_encode($productdata);
			}
			
	public function getFeaturedImage($product){
		$currentStore = $this->_storeManager->getStore();
		$mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'catalog/product';
		if( $product->getImage() != 'no_selection' ){
			return $mediaUrl.$product->getData('image');			
		}elseif( $product->getSmallImage() != 'no_selection' ){
			return $mediaUrl.$product->getData('small_image'); 
		}else{
			return $mediaUrl.$product->getData('thumbnail');
		}
	}
		
			
	public function getcategory($product){
		$categories = $product->getCategoryIds();
		//print_r($categories);
		$catearray = array();
		$newcategory = array();
		foreach($categories as $categorie){
			//echo $categorie;
			if($categorie != 2){
				$parentCategory = $this->categoryRepository->get($categorie, $this->_storeManager->getStore()->getId());
				
				$cate = strstr($parentCategory->getUrlPath(), '.', true);
				if($cate==""){
					$cate = ucwords(str_replace('-',' ',$parentCategory->getUrlPath()));
				}else{
					$cate = ucwords(str_replace('-',' ',$cate));				
				}
				$catearray[$parentCategory->getPath()] = $cate;
				$newcategory[] = array("id"=> $parentCategory->getPath(),"label"=> $cate,);
			}
		}				
		return $newcategory;
	}

	public function pushItem(){
				
		$rspAry = $this->authentication();		
		$date = $this->date->gmtDate();
		$curl1 = curl_init();
			
		if(!empty($rspAry) && isset($rspAry['auth_token']) && !empty($rspAry['auth_token'])){
			
			$auth_token = $rspAry['auth_token'];
			$headers1 = array(
				"content-type: application/json",
				"auth-token: $auth_token"
			  );
			
			$post_data1 = $this->productjson();
			
			$post_id = 0;
			
			if(empty($post_data1)){
				return NULL;
			}
			
			if(!empty($post_data1)){
			$RsenceauthModel = $this->_modelRsenceauthFactory->create();	
			  $data1 = array(
				  'request' => $post_data1,
				  'status'=> 1,
				  'flag' => 0,
				  'created_at' => $date
			  );
			  
			  $model = $RsenceauthModel->addData($data1)->save();
			  $post_id = $model->getId();
			}
			
			curl_setopt_array($curl1, array(
			  CURLOPT_URL => "https://retail-api.recosenselabs.com/v1.1/item",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $post_data1,
			  CURLOPT_HTTPHEADER => $headers1
			));
			
			$response = curl_exec($curl1);
			
			$err = curl_error($curl1);

			curl_close($curl1);
			
			if ($err) {
			  return "cURL Error #:" . $err;
			} else {
			  
			  $data1 = array(
				  'request' => $post_data1,
				  'response' => $response,
				  'status'=> 0,
				  'flag' => 1,
				  'updated_at' => $date
			  );
			  
			  if($post_id != 0){
				  $model = $RsenceauthModel->setId($post_id)->addData($data1)->save();				  
			  }else{
				  $model = $RsenceauthModel->addData($data1)->save();
			  }
			  $this->updateProductAction();
				
			  return $response;
			  
			}
			
		}
		
	}
	
	public function updateProductAction(){
		$productIds = $this->getProductIds();
		
		if(!empty($productIds)){
			foreach($productIds as $product_id){
				$RsenceproductModel = $this->_modelRsenceproductFactory->create();
				$eids = $RsenceproductModel->getCollection()->addFieldToFilter('product_id',array('eq' => $product_id)); 
				if(!empty($eids)){
					foreach($eids as $id){						
						$id = $id->getData('entity_id');
						$ndata = array(
						  'status' => 1
						);
						$RsenceproductModel->setId($id)->addData($ndata)->save();
					}
				}
			}
			
		}
	}

}

