<?php

namespace RecoSense\LabAPI\Observer;

use Magento\Framework\Event\ObserverInterface;
use RecoSense\LabAPI\Model\RsenceproductFactory;

class Recosensesectionchange implements ObserverInterface
{   
	protected $_modelRsenceproductFactory;
	protected $date;
	
	public function __construct(
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
        RsenceproductFactory $modelRsenceproductFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \RecoSense\LabAPI\Helper\Data $helper
    ) {
        $this->_modelRsenceproductFactory = $modelRsenceproductFactory;
        $this->date = $date;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {	
		shell_exec('php bin/magento PushItem:RecoSense > /dev/null 2>/dev/null &');
		//$successMessage = $this->helper->pushItem();
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $modulestatus = $this->scopeConfig->getValue('recosensesection/recosensegroup/active', $storeScope);

    }   
}

?>
