<?php

namespace RecoSense\LabAPI\Observer;

use Magento\Framework\Event\ObserverInterface;
use RecoSense\LabAPI\Model\RsenceproductFactory;

class Productsaveafter implements ObserverInterface
{   
	protected $_modelRsenceproductFactory;
	protected $date;
	
	public function __construct(
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
        RsenceproductFactory $modelRsenceproductFactory,
        \RecoSense\LabAPI\Helper\Data $helper
    ) {
        $this->_modelRsenceproductFactory = $modelRsenceproductFactory;
        $this->date = $date;
        $this->helper = $helper;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       $_product = $observer->getProduct();  // you will get product object
       $productid=$_product->getId();
       $RsenceproductModel = $this->_modelRsenceproductFactory->create();
       $collections = $RsenceproductModel->getCollection()->addFieldToFilter('product_id',array('eq' => $productid)); //Get Collection of module data
       $collections->setPageSize(1)->setCurPage(1);
       $date = $this->date->gmtDate();
       
       if(count($collections)>0){
			foreach($collections as $collection){		
				$RsenceproductModel = $this->_modelRsenceproductFactory->create();
				$ndata = array(
					  'product_id' => $productid,
					  'action' => 'update',
					  'status' => 0,
					  'update_at' => $date
				);				
				$RsenceproductModel->setId($collection->getId())->addData($ndata)->save();	
			}
			$successMessage = $this->helper->pushItem();
		}else{
			$RsenceproductModel = $this->_modelRsenceproductFactory->create();
			$ndata = array(
				  'product_id' => $productid,
				  'action' => 'insert',
				  'status' => 0,
				  'created_at' => $date
			);
			$RsenceproductModel->addData($ndata)->save();
			$successMessage = $this->helper->pushItem();
        }
    }   
}

?>
