<?php

namespace RecoSense\LabAPI\Observer;

use Magento\Framework\Event\ObserverInterface;
use RecoSense\LabAPI\Model\RsenceproductFactory;

class Productdeletebefore implements ObserverInterface
{   
	protected $_modelRsenceproductFactory;
	protected $date;
	
	public function __construct(
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
        RsenceproductFactory $modelRsenceproductFactory
    ) {
        $this->_modelRsenceproductFactory = $modelRsenceproductFactory;
        $this->date = $date;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       $_product = $observer->getProduct();  // you will get product object
       $productid=$_product->getId();
       $RsenceproductModel = $this->_modelRsenceproductFactory->create();
       $collections = $RsenceproductModel->getCollection()->addFieldToFilter('product_id',array('eq' => $productid)); //Get Collection of module data
       $collections->setPageSize(1)->setCurPage(1);
       $date = $this->date->gmtDate();
       
       if(count($collections)>0){
			foreach($collections as $collection){		
				$ndata = array(
					  'product_id' => $productid,
					  'action' => 'delete',
					  'status' => 0,
					  'update_at' => $date
				);				
				$RsenceproductModel->setId($collection->getId())->addData($ndata)->save();					
			}
		}else{
				$ndata = array(
					  'product_id' => $productid,
					  'action' => 'delete',
					  'status' => 0,
					  'created_at' => $date
				);
				$RsenceproductModel->addData($ndata)->save();
        }
    }   
}

?>
