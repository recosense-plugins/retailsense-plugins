<?php
namespace RecoSense\LabAPI\Cron;
use RecoSense\LabAPI\Model\RsenceproductFactory;
class Request {
 
	public function __construct(
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
        RsenceproductFactory $modelRsenceproductFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \RecoSense\LabAPI\Helper\Data $helper
    ) {
        $this->_modelRsenceproductFactory = $modelRsenceproductFactory;
        $this->date = $date;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
    }
    
    public function execute() {
    	try{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$tableName = $resource->getTableName('labapi_rsenceproduct'); //gives table name with prefix
			 
			$sql = 'INSERT INTO ' . $tableName .' (product_id,action,status,created_at) SELECT entity_id as product_id, "insert" as action,0 as status,now() as created_at FROM catalog_product_entity WHERE NOT EXISTS (SELECT * FROM labapi_rsenceproduct WHERE labapi_rsenceproduct.product_id = catalog_product_entity.entity_id)';
			$connection->query($sql);
			$successMessage = $this->helper->pushItem();
	        return $this;
	    }
	    catch(\Exception $e){

	    }
    }
}
