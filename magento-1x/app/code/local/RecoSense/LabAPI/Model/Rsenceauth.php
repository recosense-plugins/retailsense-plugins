<?php

class RecoSense_LabAPI_Model_Rsenceauth extends Mage_Core_Model_Abstract
{
    protected function _construct(){

       $this->_init("labapi/rsenceauth");

    }

    public function getOptionArray(){
       $options = array(
       		 Mage::helper('labapi')->__('200 ok'),
       		 Mage::helper('labapi')->__('Bad Request'),
       		 Mage::helper('labapi')->__('Unauthorized')
       	);
       
       return $options;
   }

}
	 
