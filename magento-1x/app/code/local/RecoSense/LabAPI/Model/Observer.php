<?php
class RecoSense_LabAPI_Model_Observer
{
	public $_roleids = '';
	
	public $_roleDltids = '';

	public $_mVersion = '';
	
	public $_counter = '';
	public function __construct(){
		$this->_roleids = array();
		$this->_roleDltids = array();
		$this->_mVersion = $this->GetMagentoVersion();
		$this->_counter = 0;
	}
	
	public function productjson(){
		
		$productIds = $this->getProductIds();
		
		print_r($productIds);
		
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('retailproduct');
		
		if(empty($productIds)){
			echo "empty products";
			print_r($productIds);
			return NULL;
		}

		$_productCollection = Mage::getModel('catalog/product')
								->getCollection()
								->addAttributeToSelect('*')
								->addAttributeToFilter('entity_id', array('in'=> $productIds));
								
		$productdata = array();
		
		$client_id = Mage::getStoreConfig('setting/gsetting/client_id');

		$productdata['magento_version'] = $this->_mVersion;
		$productdata['integration'] = "m1x";
		$productdata['client_id'] = $client_id;
		
		foreach ($_productCollection as $_product){
			
			$product = Mage::getModel('catalog/product')->load($_product->getId());
			$allProductData = $product->getData();
			$productType = $product->getTypeId();
			
			$categories = $this->getcategory($product);
			
			$parentIdArray = "";
			$childProducts = array();
			$configmodel = Mage::getModel('catalog/product_type_configurable');
			
			$parentIdsArray = $configmodel->getParentIdsByChild( $product->getId() );

			$attributecollection = array();

			$captureProductAttr = ["entity_id","entity_type_id","attribute_set_id","type_id","sku","has_options",
					       "required_options","created_at","updated_at","price","special_price","msrp",
					       "gift_wrapping_price","name","meta_title","meta_description","image",
					       "small_image","thumbnail","url_key","custom_design","page_layout",
					       "options_container","msrp_enabled","msrp_display_actual_price_type",
					       "gift_message_available","gift_wrapping_available","status","is_recurring",
					       "visibility","tax_class_id","description","short_description","custom_layout_update",
					       "custom_design_from","custom_design_to","group_price","group_price_changed",
					       "media_gallery","tier_price","tier_price_changed","stock_item",
					       "is_in_stock","is_salable","news_from_date","news_to_date","url_path","gallery"];
			foreach($allProductData as $key => $value){
				if (!in_array($key, $captureProductAttr) && $value) {
				        $attributecollection[] = array(
                                             "label"=> $key,
                                             "value"=>$value,
                                             "id" =>$key
					);
	                        }

			}
			if($productType == 'configurable'){  
				
				$childs = $configmodel->getUsedProductCollection($product)->addAttributeToSelect('*')->getData();
					foreach($childs as $value){
						$childid = (int)$value['entity_id'];
						$childProducts[]= (int)$value['entity_id'];
						$parentid = $product->getId();
					}
				
				$productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
				foreach($productAttributeOptions as $options)
					{
						$attributeid = $options['attribute_id'];
						$attributelabel = $options['frontend_label'];
					 
						foreach($options as $option)
						{
							foreach($option as $value)
							{
								 $attributeoption = $value['label'];
								
							}
							 
						}
						
					$attributecollection[] = array(
											"label"=> $attributelabel,
											"value"=>$attributeoption,
											"id" =>$attributeid
										);	
					}
				
			}
			$products = $readConnection->fetchAll('SELECT * FROM `catalog_product_super_link` where product_id = '.$product->getId().' Limit 0,1');
			
			$parent_id = 0;
			if(count($products))
			{
				$parent_id = $products[0]['parent_id'];
			}
			$tagcollect = array();
			$tags = $readConnection->fetchAll("SELECT `tag`.name FROM `tag` left join `tag_relation` on `tag_relation`.tag_id = `tag`.tag_id where `tag_relation`.product_id = '".$product->getId()."'");

			foreach($tags as $tag)
			{
				$tagcollect[] = $tag['name'];
				}
			
				$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
			$qty = (int) $stock->getQty();
			$product_links = array($product -> getProductUrl());
			
			$_reviews = Mage::getModel('review/review')->getResourceCollection();
			$_reviews->addStoreFilter( Mage::app()->getStore()->getId() )
			->addEntityFilter('product', $_product->getId())
			->addStatusFilter( Mage_Review_Model_Review::STATUS_APPROVED )
			->setDateOrder()
			->addRateVotes();
			$avg = 0;
			$ratings = array();
			$ratingPercent = 0;
			if (count($_reviews) > 0){
			foreach ($_reviews->getItems() as $_review):
				foreach( $_review->getRatingVotes() as $_vote ):
					$ratings[] = $_vote->getPercent();
				endforeach;
			endforeach;
			 $avg = array_sum($ratings)/count($ratings);
			 $ratingPercent = (float)number_format($avg / 20,1);
			 }
			 
			$visibility = $_product->getVisibility();
              if($visibility == 4){
                 $visibility = array('visible_in_catalog'=>true,'visible_in_search'=>true);
            }else if($visibility == 3){
                 $visibility = array('visible_in_catalog'=>false,'visible_in_search'=>false);    
            }else if($visibility == 2){
                 $visibility = array('visible_in_catalog'=>true,'visible_in_search'=>false);    
            }else if($visibility == 1){
                 $visibility = array('visible_in_catalog'=>false,'visible_in_search'=>false);    
            }
			
			$productdata['items'][]= 
			array(
				"item_id"=>$_product->getId(),
				"title"=>$_product->getName(),
				"long_description"=>$_product->getDescription(),
				"short_description"=>$_product->getShortDescription(),
				"parent_id"=> (int)$parent_id,
				"associated_item_ids"=> $childProducts,
				"sku"=> $_product->getSku(),
				"price"=> (float)$_product->getFinalPrice(),
				"category_trees"=>$categories,	
				"visibility"=>$visibility,
				"product_links" =>$product_links,
				"product_attributes"=> $attributecollection,
				"rating"=> $ratingPercent,
				"in_stock"=> $qty,
				"tags"=>$tagcollect,
				"images"=> array(
					"base_image"=> Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getImage()),
					"small_image"=> Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getSmallImage()),
					"thumbnail"=>  Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getThumbnail()),
					"featured_image"=> $this->getFeaturedImage($product)
				),
			);
			
			$productdata['action']= "update";
		   
		}
		return json_encode($productdata);
	}

	public function getcategory($product){
		$productCats = $product->getCategoryIds();
		$categories = $this->getCatdiffids($productCats);
		
		if(empty($categories)){
			$categories = $productCats;
		}
		$catearray=array();
		$newcategory = array();
		foreach($categories as $categorie){			
			if($categorie != 2){
				$parentCategory = Mage::getModel('catalog/category')->load($categorie);
				$cate = strstr($parentCategory->getUrlPath(), '.', true);
				$cate = ucwords(str_replace('-',' ',$cate));				
				$catearray[$parentCategory->getPath()] = $cate;
				$newcategory[] = array("id"=> $parentCategory->getPath(),"label"=> $cate,);
			}
		}
		
		return $newcategory;
	}
	
	public function getCatdiffids($categories){
		
		$perentids = array();
		
		if(!empty($categories)){
			foreach($categories as $categorie){			
				if($categorie != 2){
					$parentCategory = Mage::getModel('catalog/category')->load($categorie)->getParentCategory();
					$perentids[] = $parentCategory->getId();
				}
			}
		}
		
		$catarr = array();
		
		if(!empty($perentids) && !empty($categories)){
			$catarr = array_diff($categories, $perentids);
		}
		
		return $catarr;
	}
	
	// Authentication Token
	public function authentication(){
		
		$curl = curl_init();
		$enable_module = Mage::getStoreConfig('setting/gsetting/enable_module');
		$client_id = Mage::getStoreConfig('setting/gsetting/client_id');
		$client_secret = Mage::getStoreConfig('setting/gsetting/client_secret_id');
		$grant_type = 'auth_token';
		
		if( $enable_module == 0 || $client_id == '' || $client_secret == '' || $grant_type == '' ){
			return NULL;
		}

		$post_data = array(
				'integration' => $this->_mVersion,
				'client_id' => $client_id,
				'client_secret' => $client_secret,
				'grant_type' => $grant_type
			);	
		 
		 $headers = array(
			"content-type: application/json"
		  );


		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://retail-api.recosenselabs.com/v1.1/oauth2",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $post_data
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		$rspAry = array();

		if ($err) {
		  $rspAry = "cURL Error #:" . $err;
		} else {
		  $rspAry = json_decode($response, true);
		}
		
		return $rspAry;
	}
	
	// Push Item API
	public function pushItem(){
		$rspAry = $this->authentication();
		
		if(empty($rspAry)){
			
			  $data1 = array(
				  'response' => 'Authentication Fail',
				  'status'=> 2,
				  'flag' => 0,
				  'created_at' => now()
			  );
			  
			  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  $post_id = $model->getId();
			  
			  return null;
		}
		
		
		
		if(isset($rspAry['error'])){
			$data1 = array(
				  'response' => $rspAry['error'],
				  'status'=> 2,
				  'flag' => 0,
				  'created_at' => now()
			  );
			  
			  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  $post_id = $model->getId();
			  
			  return null;
		}
		
		$curl1 = curl_init();
		

		if(!empty($rspAry) && isset($rspAry['auth_token']) && !empty($rspAry['auth_token'])){

			$auth_token = $rspAry['auth_token'];

			$headers1 = array(
				"content-type: application/json",
				"auth-token: $auth_token"
			  );
			
			$post_data1 = $this->productjson();
			$post_id = 0;
			
			if(empty($post_data1)){
				return NULL;
			}
			
			if(!empty($post_data1)){
			  $data1 = array(
				  'request' => $post_data1,
				  'status'=> 3,
				  'flag' => 0,
				  'created_at' => now()
			  );
			  
			  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  $post_id = $model->getId();
			}
			curl_setopt_array($curl1, array(
			  CURLOPT_URL => "https://retail-api.recosenselabs.com/v1.1/item",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $post_data1,
			  CURLOPT_HTTPHEADER => $headers1
			));
			
			$response = curl_exec($curl1);
			$httpcode = curl_getinfo($curl1, CURLINFO_HTTP_CODE);
			$err = curl_error($curl1);
			curl_close($curl1);
			$status = 0;
			if($httpcode == 200){
				$status = 0;
			}elseif($httpcode == 400){
				$status = 1;
			}else{
				$status = 2;
			}
			
			if ($err) {
			  return "cURL Error #:" . $err;
			} else {

			  $data1 = array(
				  'request' => $post_data1,
				  'response' => $response,
				  'status'=> $status,
				  'flag' => 1,
				  'updated_at' => now()
			  );
			  
			  if($post_id != 0){
				  $model = Mage::getModel("labapi/rsenceauth")->setId($post_id)->addData($data1)->save();
			  }else{
				  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  }
			  
			  if($status == 0){
			  	$this->updateProductAction($response);
			  }
				
			  return $response;
			  
			}
			
		}
		
	}
	
	
	// Delete Item API
	public function delteItem(){
		
		$rspAry = $this->authentication();
		
		if(empty($rspAry)){
			
			  $data1 = array(
				  'response' => 'Authentication Fail',
				  'status'=> 2,
				  'flag' => 0,
				  'created_at' => now()
			  );
			  
			  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  $post_id = $model->getId();
			  
			  return null;
		}
		
		if(isset($rspAry['error'])){
			$data1 = array(
				  'response' => $rspAry['error'],
				  'status'=> 2,
				  'flag' => 0,
				  'created_at' => now()
			  );
			  
			  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  $post_id = $model->getId();
			  
			  return null;
		}
		
		$curl2 = curl_init();
		
		$productIds = $this->getDltProductIds();
		
		if(empty($productIds)){
			return NULL;
		}

		if(!empty($rspAry) && isset($rspAry['auth_token']) && !empty($rspAry['auth_token'])){

		  $auth_token = $rspAry['auth_token'];

		  $client_id = Mage::getStoreConfig('setting/gsetting/client_id');

		  $post_data2 = array (
		  	'integration' => $this->_mVersion,
			'client_id' => $client_id,
			'action' => 'delete',
			'items' => $productIds,
		  );

		  $post_data2 = json_encode($post_data2);
		  
		  if(!empty($post_data2)){
			  $data1 = array(
				  'request' => $post_data2,
				  'status'=> 3,
				  'flag' => 0,
				  'created_at' => now()
			  );
			  
			  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  $post_id = $model->getId();
			}
		 
		  $headers2 = array(
			"content-type: application/json",
			"auth-token: $auth_token"
		  );

		  curl_setopt_array($curl2, array(
			  CURLOPT_URL => "https://retail-api.recosenselabs.com/v1.1/item",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $post_data2,
			  CURLOPT_HTTPHEADER => $headers2
			));

		  $response = curl_exec($curl2);
		  $httpcode = curl_getinfo($curl2, CURLINFO_HTTP_CODE);
		  $err = curl_error($curl2);
		  curl_close($curl2);
		  
		  $status = 0;
		  if($httpcode == 200){
			 $status = 0;
		  }elseif($httpcode == 400){
			$status = 1;
		  }else{
			$status = 2;
		  }

		  $rspAry = array();

		  if ($err) {
			return "cURL Error #:" . $err;
		  } else {
			
			$data1 = array(
				  'request' => $post_data2,
				  'response' => $response,
				  'status'=> $status,
				  'flag' => 1,
				  'updated_at' => now()
			  );
			
			 if($post_id != 0){
				  $model = Mage::getModel("labapi/rsenceauth")->setId($post_id)->addData($data1)->save();
			  }else{
				  $model = Mage::getModel("labapi/rsenceauth")->addData($data1)->save();
			  }
			
			if($status == 0){
				$this->updateDltProductAction($response);
			}

			return $rspAry = json_decode($response, true);
		  }

		}				
	}

	public function afterSaveproduct(Varien_Event_Observer $observer){
		
		$product = $observer->getData('product');

		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('retailproduct');
		$products = $readConnection->fetchAll('SELECT product_id,entity_id FROM retailproduct where product_id ='.$product->getId().' Limit 0,1');
		if(!empty($products)){
			
			$ndata = array(
				  'product_id' => $product->getId(),
				  'action' => 'update',
				  'status' => 0,
				  'created_at' => now()
			);
			$model = Mage::getModel("labapi/rsenceproduct")
			->setId($products[0]['entity_id'])
			->addData($ndata)->save();
			
				
		}else{
		
			$ndata = array(
				  'product_id' => $product->getId(),
				  'action' => 'insert',
				  'status' => 0,
				  'created_at' => now()
			);
			$model = Mage::getModel("labapi/rsenceproduct")->addData($ndata)->save();
		}
		
	}
	
	public function beforedeleteproduct(Varien_Event_Observer $observer){
		
		$product = $observer->getData('product');
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$products = $readConnection->fetchAll('SELECT product_id,entity_id FROM retailproduct where product_id ='.$product->getId().' Limit 0,1');
		if(!empty($products)){
			
			$ndata = array(
				  'product_id' => $product->getId(),
				  'action' => 'delete',
				  'status' => 0,
				  'created_at' => now()
			);
			$model = Mage::getModel("labapi/rsenceproduct")
			->setId($products[0]['entity_id'])
			->addData($ndata)->save();
			
				
		}else{
		
			$ndata = array(
				  'product_id' => $product->getId(),
				  'action' => 'delete',
				  'status' => 0,
				  'created_at' => now()
			);
			$model = Mage::getModel("labapi/rsenceproduct")->addData($ndata)->save();
		}
	}
	
	public function getProductIds(){
		$fcounter = $this->_counter;
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$query = 'SELECT product_id FROM retailproduct where status = 0 AND action != "delete" LIMIT 50 offset '.$fcounter.'';
        $products = $readConnection->fetchAll($query);
		if(!empty($products)){
			$this->_roleids = array_column($products, 'product_id');
		}else{
			$this->_roleids = NULL;
		}
		$this->_counter = $this->_counter+50;
		return $this->_roleids;
		
	}
	
	public function getDltProductIds(){
		
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$products = $readConnection->fetchAll("SELECT product_id FROM retailproduct where status = 0 AND action = 'delete' LIMIT 0,50");
		if(!empty($products)){
			$this->_roleDltids = array_column($products, 'product_id');
		}else{
			$this->_roleDltids = NULL;
		}
		
		return $this->_roleDltids;
	}
	
	public function updateProductAction($response){

		$res = json_decode($response,true);
		$item_ids = array();
		if(isset($res['exception_message']) && !empty($res['exception_message'])){
			 $item_ids = array_column($res['exception_message'], 'item_id');
		}

		if(!empty($this->_roleids)){
			foreach($this->_roleids as $product_id){
				$resource = Mage::getSingleton('core/resource');
				$readConnection = $resource->getConnection('core_read');
				if(!in_array($product_id, $item_ids)){
					$eids = $readConnection->fetchAll("SELECT entity_id FROM retailproduct where product_id =  $product_id");
					if(!empty($eids)){
						foreach($eids as $id){
							$ndata = array(
								'status' => 1
							);
							$model = Mage::getModel("labapi/rsenceproduct")->setId($id)->addData($ndata)->save();
						}
					}
				}
			}
		}
	}
	
	public function updateDltProductAction($response){
		
		$res = json_decode($response,true);
		$item_ids = array();
		if(isset($res['exception_message']) && !empty($res['exception_message'])){
			 $item_ids = array_column($res['exception_message'], 'item_id');
		}

		if(!empty($this->_roleDltids)){
			foreach($this->_roleDltids as $product_id){
				$resource = Mage::getSingleton('core/resource');
				$readConnection = $resource->getConnection('core_read');
				if(!in_array($product_id, $item_ids)){
					$eids = $readConnection->fetchAll("SELECT entity_id FROM retailproduct where product_id =  $product_id");
					if(!empty($eids)){
						foreach($eids as $id){
							$ndata = array(
							  'status' => 1
							);
							$model = Mage::getModel("labapi/rsenceproduct")->setId($id)->addData($ndata)->save();
						}
					}
				}
			}
		}
		
	}
	public function insertAllProduct(){
          shell_exec('php RetailSensescript.php > /dev/null 2>/dev/null &');
	}
	public function ShellinsertAllProduct(){
                
		  $resource = Mage::getSingleton('core/resource');
		  $readConnection = $resource->getConnection('retailproduct');
		  $writeConnection = $resource->getConnection('core_write');
		  $tableName = $resource->getTableName('retailproduct');
		  $query = 'INSERT INTO ' . $tableName .' (product_id,action,status,created_at) SELECT entity_id as product_id, "insert" as action,0 as status,now() as created_at FROM catalog_product_entity WHERE NOT EXISTS (SELECT * FROM retailproduct WHERE retailproduct.product_id = catalog_product_entity.entity_id)';

		  $test1 = $writeConnection->query($query);
		  
		  $recordcount = "SELECT COUNT(*) FROM retailproduct where status=0";
		  $test1 = $writeConnection->fetchAll($recordcount);
		  if($test1[0]['COUNT(*)'] > 0 ){
			  $records = $test1[0]['COUNT(*)'];
			  $loop = ceil($records/50);
			  for($i=1;$i<=$loop;$i++){
				  $this->pushItem();
			  }
		  }
	}
	public function updateNotSyncedProduct(){
                
		  $resource = Mage::getSingleton('core/resource');
		  $readConnection = $resource->getConnection('retailproduct');
		  $writeConnection = $resource->getConnection('core_write');
		  $tableName = $resource->getTableName('retailproduct');
		  $recordcount = "SELECT COUNT(*) FROM retailproduct where status=0";
		  $test1 = $writeConnection->fetchAll($recordcount);
		  if($test1[0]['COUNT(*)'] > 0 ){
			  $records = $test1[0]['COUNT(*)'];
			  $loop = ceil($records/50);
			  for($i=1;$i<=$loop;$i++){
				  echo $i."--";
				  $this->pushItem();
			  }
		  }
	}
	


	public function getFeaturedImage($product){

		if( $product->getImage() != 'no_selection' ){
			return Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getImage());
		}elseif( $product->getSmallImage() != 'no_selection' ){
			return Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getSmallImage());
		}else{
			return Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getThumbnail());
		}
	}

	public function GetMagentoVersion(){
		return 'm'.Mage::getVersion();
	}
}
