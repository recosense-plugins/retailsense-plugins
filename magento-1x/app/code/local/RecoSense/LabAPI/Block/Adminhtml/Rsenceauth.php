<?php


class RecoSense_LabAPI_Block_Adminhtml_Rsenceauth extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_rsenceauth";
	$this->_blockGroup = "labapi";
	$this->_headerText = Mage::helper("labapi")->__("Request Manager");
	$this->_addButtonLabel = Mage::helper("labapi")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}
