<?php
class RecoSense_LabAPI_Block_Adminhtml_Rsenceproduct_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("labapi_form", array("legend"=>Mage::helper("labapi")->__("Item information")));

				
						$fieldset->addField("action", "text", array(
						"label" => Mage::helper("labapi")->__("Action"),
						"name" => "action",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getRsenceproductData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getRsenceproductData());
					Mage::getSingleton("adminhtml/session")->setRsenceproductData(null);
				} 
				elseif(Mage::registry("rsenceproduct_data")) {
				    $form->setValues(Mage::registry("rsenceproduct_data")->getData());
				}
				return parent::_prepareForm();
		}
}
