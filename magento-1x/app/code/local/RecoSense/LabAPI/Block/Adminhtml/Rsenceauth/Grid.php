<?php

class RecoSense_LabAPI_Block_Adminhtml_Rsenceauth_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("rsenceauthGrid");
				$this->setDefaultSort("entity_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("labapi/rsenceauth")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("entity_id", array(
				"header" => Mage::helper("labapi")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "entity_id",
				));
                
				$this->addColumn("request", array(
				"header" => Mage::helper("labapi")->__("Request"),
				"index" => "request",
				"type"  => "text",      
				"width" => "400px",
				"sortable"  =>false,
				"filter" => false,
				));
				
				$this->addColumn("response", array(
				"header" => Mage::helper("labapi")->__("Response"),
				"index" => "response",
				"type"  => "text",
				"width" => "400px",     
				"sortable"  =>false,
				"filter" => false
				));
				
				$this->addColumn('created_at', array(
					'header'    => Mage::helper('labapi')->__('Created at'),
					'width' => '150px',
					'type'      => 'datetime',
					'align'     => 'center',
					'index'     => 'created_at',
					'gmtoffset' => true
				));
				
				$this->addColumn('updated_at', array(
					'header'    => Mage::helper('labapi')->__('Updated at'),
					'width' => '150px',
					'type'      => 'datetime',
					'align'     => 'center',
					'index'     => 'updated_at',
					'gmtoffset' => true
				));
				
				$this->addColumn('status',
			       array(
			           'header'=> Mage::helper('labapi')->__('Status'),
			           'width' => '70px',
			           'index' => 'status',
			           'type'  => 'options',
			           'options' => Mage::getModel('labapi/rsenceauth')->getOptionArray(),
			   ));
				$this->addColumn('flag',
			       array(
			           'header'=> Mage::helper('labapi')->__('Flag'),
			           'width' => '70px',
			           'index' => 'flag',
			           'type'  => 'options',
			           'options' =>  Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
			   ));
				return parent::_prepareColumns();
		}
				
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('entity_id');
			$this->getMassactionBlock()->setFormFieldName('entity_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_rsenceauth', array(
					 'label'=> Mage::helper('labapi')->__('Remove Rsenceauth'),
					 'url'  => $this->getUrl('*/adminhtml_rsenceauth/massRemove'),
					 'confirm' => Mage::helper('labapi')->__('Are you sure?')
				));
			return $this;
		}
}
