<?php
class RecoSense_LabAPI_Block_Adminhtml_Rsenceauth_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("rsenceauth_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("labapi")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("labapi")->__("Item Information"),
				"title" => Mage::helper("labapi")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("labapi/adminhtml_rsenceauth_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
