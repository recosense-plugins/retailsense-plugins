<?php


class RecoSense_LabAPI_Block_Adminhtml_Rsenceproduct extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct(){

	$this->_controller = "adminhtml_rsenceproduct";
	$this->_blockGroup = "labapi";
	$this->_headerText = Mage::helper("labapi")->__("Product Action Manager");
	$this->_addButtonLabel = Mage::helper("labapi")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	
	}

}
