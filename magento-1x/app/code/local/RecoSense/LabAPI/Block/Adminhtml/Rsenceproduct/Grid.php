<?php

class RecoSense_LabAPI_Block_Adminhtml_Rsenceproduct_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct(){
				parent::__construct();
				$this->setId("rsenceproductGrid");
				$this->setDefaultSort("entity_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection(){
				$collection = Mage::getModel("labapi/rsenceproduct")->getCollection();
				$this->setCollection($collection);
				
				return parent::_prepareCollection();
		}
		
		protected function _prepareColumns(){
			
				$this->addColumn("entity_id", array(
				"header" => Mage::helper("labapi")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "entity_id",
				));
				
				$this->addColumn("product_id", array(
				"header" => Mage::helper("labapi")->__("Product ID"),
				"index" => "product_id",
				));
                
				$this->addColumn("action", array(
				"header" => Mage::helper("labapi")->__("Action"),
				"index" => "action",
				));
				
				$this->addColumn('created_at', array(
					'header'    => Mage::helper('labapi')->__('Created at'),
					'width' => '180px',
					'type'      => 'datetime',
					'align'     => 'center',
					'index'     => 'created_at',
					'gmtoffset' => true
				));	
				
				$this->addColumn('status',
			       array(
			           'header'=> Mage::helper('labapi')->__('Status'),
			           'width' => '70px',
			           'index' => 'status',
			           'type'  => 'options',
			           'options' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
			   ));

				return parent::_prepareColumns();
		}
		
		protected function _prepareMassaction(){
			
			$this->setMassactionIdField('entity_id');
			$this->getMassactionBlock()->setFormFieldName('entity_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_rsenceproduct', array(
					 'label'=> Mage::helper('labapi')->__('Remove Rsenceproduct'),
					 'url'  => $this->getUrl('*/adminhtml_rsenceproduct/massRemove'),
					 'confirm' => Mage::helper('labapi')->__('Are you sure?')
				));
			return $this;
		}
			

}
