<?php
class RecoSense_LabAPI_Block_Adminhtml_Rsenceproduct_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("rsenceproduct_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("labapi")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("labapi")->__("Item Information"),
				"title" => Mage::helper("labapi")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("labapi/adminhtml_rsenceproduct_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
