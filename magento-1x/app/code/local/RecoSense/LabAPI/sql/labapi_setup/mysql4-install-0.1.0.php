<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table retailauth(
	entity_id int not null auto_increment,
	request text,
	response text,
	status int(10),
	flag int(10),
    created_at timestamp NULL DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
	primary key(entity_id)
);
SQLTEXT;

$installer->run($sql);

$sql1=<<<SQLTEXT
create table retailproduct(
	entity_id int not null auto_increment,
	product_id int(10),
    action text,
    status int(10),
    created_at timestamp NULL DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL,
	primary key(entity_id)
);
SQLTEXT;

$installer->run($sql1);
$installer->endSetup();
	 
