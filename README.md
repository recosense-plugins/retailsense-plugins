Recosenselabs Lab API Extension


Facts<br>
version: 0.1.0<br>
extension on GitLab<br>
git@gitlab.com:recosense-plugins/retailsense-plugins.git<br>


Description


Requirements
PHP >= 5.6.0
REST


Compatibility<br>
Magento >= 2


Installation Instructions<br>
Install modman on the server<br>
1. git clone https://github.com/colinmollenhour/modman.git<br>
2. cd modman && sudo cp modman /usr/bin<br>


Run below command<br>
1. modman init<br>
2. modman clone retailsense-plugins  https://gitlab.com/recosense-plugins/retailsense-plugins.git<br>
3. Clear the cache System > Cache Management > Flush Magento Cache, logout from the admin panel and then login again.<br>
4. Configure and activate the extension under System > Configuration > recosenselabs<br>


Uninstallation<br>
Remove all extension files from your Magento installation


Support<br>
If you have any issues with this extension, open an issue on GitHub.


Developer
Recosenselabs 
http://recosenselabs.com/


License
OSL - Open Software Licence 3.0


Copyright
(c) 2018 recosenselabs
